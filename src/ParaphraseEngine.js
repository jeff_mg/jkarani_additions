const fs = require('fs')
let Paraphrase = require('paraphrase-sentences')
const key = 'AIzaSyBsJTVQDo7VeDP096rET7EZGVZj76tXs8E'

let paraphrase = new Paraphrase({ key })

class ParaphraseEngine {
    ParaphraseEngine() {
      this.paraphraseQuestion = this.paraphraseQuestion.bind(this)
      this.multiRephrase = this.multiRephrase.bind(this)
    }

    paraphraseQuestion(faq){
        return new Promise(async (resolve,reject)=>{
            const question = faq.question[0]
            let results = await paraphrase.get(question)
            if(Array.isArray(results)){
                console.log('Successfully Done With: ',question)
                resolve(results)
            }else{
                console.log('Failiurly Done With: ',question)
                reject([{error:'Result was not array',question}])
            }
        })
    }

    multiRephrase(dataSource){
        return new Promise((resolve,reject)=>{
            const sampleData = dataSource.slice(0,2)
            const processedFaqs = []
            let counter = 0
            
            setInterval(() => {
                const faq = dataSource[counter]
                counter++
            
                paraphraseQuestion(faq).then((result)=>{
                    const rephrased_questions = faq.question.concat(result)
                    const original_question = faq.question[0]
                    processedFaqs.push( { original_question, rephrased_questions, answer: faq.answer } )
                    if(counter >= dataSource.length){
                        fs.writeFileSync( `${__dirname}/results/extended_kra_faqs.json`, JSON.stringify(processedFaqs) )
                        resolve(true)
                    }
                }).catch((error)=>{
                    console.log('The following error occured: ',error)
                    if(counter >= dataSource.length){
                        fs.writeFileSync( `${__dirname}/results/extended_kra_faqs.json`, JSON.stringify(processedFaqs) )
                        reject(false)
                    }
                })
            }, 3000)
        })
    }
}

module.exports = ParaphraseEngine
 





