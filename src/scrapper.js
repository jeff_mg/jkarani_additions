const puppeteer = require('puppeteer')
const fs = require('fs')

const blockedResourceTypes = [
  'image',
  'media',
  'font',
  'texttrack',
  'object',
  'beacon',
  'csp_report',
  'imageset',
];

const skippedResources = [
  'quantserve',
  'adzerk',
  'doubleclick',
  'adition',
  'exelator',
  'sharethrough',
  'cdn.api.twitter',
  'google-analytics',
  'googletagmanager',
  'google',
  'fontawesome',
  'facebook',
  'analytics',
  'optimizely',
  'clicktale',
  'mixpanel',
  'zedo',
  'clicksor',
  'tiqcdn',
];

const getFaqs = async firstUrl => new Promise(async (resolve, reject) => {
  // Instantiate a browser instance
  const browser = await puppeteer.launch({
    headless: true,
    // executablePath: 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe',
  })

  const getFaqsFromPage = async (url) => {
    try {
      console.log('starting scrapper')
      const page = await browser.newPage()

      await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36')

      await page.setRequestInterception(true);

      page.on('request', (request) => {
        const requestUrl = request._url.split('?')[0].split('#')[0];
        if (
          blockedResourceTypes.indexOf(request.resourceType()) !== -1
            || skippedResources.some(resource => requestUrl.indexOf(resource) !== -1)
        ) {
          request.abort();
        } else {
          request.continue();
        }
      })

      await page.goto(url, { timeout: 40000 })

      await page.screenshot({ fullPage: true, path: `${__dirname}/img/screenshot.png` })

      console.log(`Begining evaluation of ${url}`)

      const faqItems = await page.evaluate(() => {
        // eslint-disable-next-line no-undef
        const faq_nodes = Array.from(document.querySelectorAll('.grid-item'))
        const faq_objects = faq_nodes.map((faq) => {
          const faq_question = faq.querySelector('.ui-accordion-header').innerText.trim()
          const faq_answer = faq.querySelector('.ui-accordion-content').innerText.trim()
          return { question: faq_question, answer: faq_answer }
        })
        return faq_objects
      })

      const nextPage = parseInt(url.split('=')[1], 10) + 20
      const nextUrl = `${url.split('=')[0]}=${nextPage}`

      if (nextPage === 220) {
        console.log(`Done evaluation of ${url}`)
        console.log('Closing down scrapper!! WOOO HOOO!!')
        await page.close()
        return faqItems
      }
      console.log(`Done evaluation of ${url}`)
      await page.close()


      return faqItems.concat(await getFaqsFromPage(nextUrl))
    } catch (error) {
      console.log('Caught an error and aborting: ', error)
      return {}
    }
  }

  const result = await getFaqsFromPage(firstUrl)

  if (Array.isArray(result)) {
    console.log('Action successfully completed : Closing Browser')
    await browser.close()
    resolve(result)
  } else {
    console.log('Action Failurely completed :Closing Browser')
    await browser.close()
    reject(result)
  }
})

module.exports = { getFaqs }
