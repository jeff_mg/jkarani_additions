/* eslint-disable class-methods-use-this */
const fs = require('fs')
const _ = require('lodash')
const createCsvWriter = require('csv-writer').createObjectCsvWriter;


class CSV_Generator {
  CSV_Generator() {
    this.generateCSV = this.generateCSV.bind(this)
  }

  generateCSV(json_faqs, output) {
    const csvWriter = createCsvWriter({
      path: `${__dirname}/results/${output}.csv`,
      header: [
        { id: 'question', title: 'question' },
        { id: 'action', title: 'action' },
        { id: 'answer', title: 'answer' },
        { id: 'answer2', title: 'answer2' },
        { id: 'category', title: 'category' },
      ],
    })

    const csv_ready_aray = []
    let counter = 0
    json_faqs.forEach((faq) => {
      counter += 1
      faq.rephrased_questions.forEach((question) => {
        csv_ready_aray.push({
          question,
          action: 'text',
          answer: faq.answer,
          answer2: '',
          category: 'global',
        })
        if (counter >= json_faqs.length) {
          csvWriter.writeRecords(csv_ready_aray)
            .then(() => {
              console.log('...Done')
              process.exit()
            })
        }
      })
    })
  }
}

module.exports = CSV_Generator
