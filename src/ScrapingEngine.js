/* eslint-disable class-methods-use-this */
const fs = require('fs')
const _ = require('lodash')
const scrapper = require('./scrapper')


const source_urls_arr = [
  {
    category_name: 'KRA_FAQS',
    initial_url: 'https://www.kra.go.ke/en/helping-tax-payers/faqs?start=0',
  },
]
class ScrapingEngine {
  ScrapingEngine() {
    this.getFaqDetails = this.getFaqDetails.bind(this)
  }

  getFaqDetails(index) {
    scrapper.getFaqs(source_urls_arr[index].initial_url).then((faqs) => {
      const arraylized_faqs = faqs.map((faq, indx) => ({
        question: [faq.question],
        answer: faq.answer,
      }))
      fs.writeFileSync(`${__dirname}/results/${source_urls_arr[index].category_name}.json`, JSON.stringify(_.uniqBy(arraylized_faqs, 'answer')))
      return true
    }).catch((err) => {
      console.log('Received an error: ', err)
      return false
    })
  }
}

module.exports = ScrapingEngine
