const fs = require('fs')
const path = require('path')
const ScrapingEngine = require('./src/ScrapingEngine')
const ParaphraseEngine = require('./src/ParaphraseEngine')
const CSV_Generator = require('./src/CSV_Generator')

const initialDataSource = require('./src/results/KRA_FAQS.json')
const extendedDataSource = require('./src/results/extended_kra_faqs.json')



const Scrapper = new ScrapingEngine()
const Paraphraser = new ParaphraseEngine()
const CSV_Gen = new CSV_Generator()


// Scrapper.getFaqDetails(0)
// Paraphraser.multiRephrase(initialDataSource).then((response)=>{
//     console.log('Rephrasing questions succeeded with status: ',response)
// }).catch((error)=>{
//     console.log('Rephrasing questions failed with status: ',response)
// })
CSV_Gen.generateCSV(extendedDataSource, 'KRA_QNA')
