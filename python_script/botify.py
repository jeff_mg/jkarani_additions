def no_inject():
	#open the file for writing
	json_write=open('botify.json','w')
	json_read=open('extended_kra_faqs.json','r')
	count=0
	#start the json file
	#the json start [ delimeter is ommitted to allow for
	#splitting // ensure to add it manually at the start of the botify.json when importing to botpress


	for line in json_read:
		if line=="{\n":
			print("This is the beginning of the object")
			json_write.write(line)
		elif "original_question" in line:
			cache=line
			newline=line.replace("original_question","id")
			newcache=cache.replace("        \"original_question\": ","")
			#write the id
			json_write.write(newline)
			#write the data header
			data="\"data\":{\n"
			json_write.write(data)
			#write the questions header
			questions="\"questions\":{\n"
			json_write.write(questions)
			#write the language header
			language="\"en\":[\n"
			json_write.write(language)
			json_write.write(newcache)
		elif "rephrased_questions" in line:
			print("got the rephrased questions")
		elif "        ],\n" in line:
			print("end of questions, closing the question")
			json_write.write("]")
			json_write.write("},\n")
			#write the answer object
			answers="\"answers\":{\n"
			json_write.write(answers)
			#write the language object
			language="\"en\":[\n"
			json_write.write(language)
		elif "answer" in line:
			newanswer=line.replace("        \"answer\": ","")
			json_write.write(newanswer)
			#close the answer
			close="]\n"
			close_one="},"
			json_write.write(close)
			json_write.write(close_one)
			action="\"action\":\"text\",\n"
			category="\"category\":\"global\",\n"
			enabled="\"enabled\" :true\n"
			json_write.write(action)
			json_write.write(category)
			json_write.write(enabled)
			json_write.write("}")

		elif line =="    },\n":
			#close the data
			json_write.write(line)
		elif line=="    }\n":
			#close the data
			json_write.write(line)
		else:
			json_write.write(line)


	json_split=open('botify.json','r')
	print("splitting the generated botify.json")
	count=0
	file=0
	file_name="import_"+str(file)+".json"
	file_open=open(file_name,'w')
	#the json delimiter
	file_open.write("[")
	for line in json_split:
		if count<=30:
			if "id" in line:
				print("found id, increasing count")
				count+=1
				file+=1
				file_open.write(line)
			else:
				file_open.write(line)


		elif line!="}    },\n":
			file_open.write(line)


		else:

			##end the json file
			file_open.write("}\n")
			file_open.write("}\n")
			file_open.write("]\n")
			##create the next json for the next iteration
			
			count=0
			file_name="import_"+str(file)+".json"
			file_open=open(file_name,'w')
			#the json delimiter
			file_open.write("[")

def inject():
	#open the file for writing
	json_write=open('botify_inject.json','w')
	json_read=open('extended_kra_faqs.json','r')
	count=0
	intent_count=0
	#start the json file
	#the json start [ delimeter is ommitted to allow for
	#splitting // ensure to add it manually at the start of the botify.json when importing to botpress


	for line in json_read:
		if intent_count==0:
			if line=="{\n":
				print("This is the beginning of the object")
				json_write.write(line)
			elif "original_question" in line:
				cache=line
				newline=line.replace("original_question","id")
				newcache=cache.replace("        \"original_question\": ","")
				#write the id
				json_write.write(newline)
				#write the data header
				data="\"data\":{\n"
				json_write.write(data)
				#write the questions header
				questions="\"questions\":{\n"
				json_write.write(questions)
				#write the language header
				language="\"en\":[\n"
				json_write.write(language)
				json_write.write(newcache)
				
				print("Found the following question:\n%s"%newcache)
				print("This is the injection point..")
				
				accept=True
				while accept:
					intent=input("Supply the new intent: ")
					comma_intent="\""+str(intent)+"\""+",\n"
					json_write.write(comma_intent) 
					print("Intent injected..\n")
					done=input("Inject more intents?[Press Enter to continue or type no to move to next question] ")
					if done=="no":
						accept=False
				


			elif "rephrased_questions" in line:
				print("got the rephrased questions")
			elif "        ],\n" in line:
				print("end of questions, closing the question")
				json_write.write("]")
				json_write.write("},\n")
				#write the answer object
				answers="\"answers\":{\n"
				json_write.write(answers)
				#write the language object
				language="\"en\":[\n"
				json_write.write(language)
			elif "answer" in line:
				newanswer=line.replace("        \"answer\": ","")
				json_write.write(newanswer)
				#close the answer
				close="]\n"
				close_one="},"
				json_write.write(close)
				json_write.write(close_one)
				action="\"action\":\"text\",\n"
				category="\"category\":\"global\",\n"
				enabled="\"enabled\" :true\n"
				json_write.write(action)
				json_write.write(category)
				json_write.write(enabled)
				json_write.write("}")

			elif line =="    },\n":
				#close the data
				json_write.write(line)
				intent_count+=1
			elif line=="    }\n":
				#close the data
				json_write.write(line)

			else:
				json_write.write(line)
		else:
			cont=input('Exit the intent injection and save current data[Yes/No]')
			if cont=="Yes":
				json_write.close()
				break
				print('Exited the injection and saved data')
			else:
				intent_count=0




	json_split=open('botify_inject.json','r')
	print("splitting the generated botify_inject.json")
	count=0
	file=0
	file_name="import_"+str(file)+"_inject"+".json"
	file_open=open(file_name,'w')
	#the json delimiter
	file_open.write("[")
	for line in json_split:
		if count<=30:
			if "id" in line:
				print("found id, increasing count")
				count+=1
				file+=1
				file_open.write(line)
			else:
				file_open.write(line)


		elif line!="}    },\n":
			file_open.write(line)


		else:

			##end the json file
			file_open.write("}\n")
			file_open.write("}\n")
			file_open.write("]\n")
			##create the next json for the next iteration
			
			count=0
			file_name="import_"+str(file)+"_inject"+".json"
			file_open=open(file_name,'w')
			#the json delimiter
			file_open.write("[")

print("Two modes are available \'intent inject\' and \'none intent inject\'")
print("To use \'intent inject\' type i on input ")
print("To use \'none intent inject\' type none on input ")
mode=input("Which mode do you want to use? ")
if mode=="i":
	inject()
else:
	no_inject()